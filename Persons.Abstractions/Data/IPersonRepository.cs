﻿using System;
using Persons.Abstractions.Entities;

namespace Persons.Abstractions.Data
{
    public interface IPersonRepository
    {
        Person Find(Guid id);
        void Insert(Person item);
    }
}
