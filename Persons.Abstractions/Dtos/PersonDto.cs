﻿using System;
using Persons.Abstractions.Entities;

namespace Persons.Abstractions.Dtos
{
    public class PersonDto
    {
        public string Name { get; }
        public Guid Id { get; }
        public string BirthDay { get; }
        public int Age { get; }

        public PersonDto(Person person)
        {
            Name = person.Name;
            Id = person.Id;
            BirthDay = person.BirthDay.Date.ToShortDateString();
            Age = person.Age;
        }
    }
}
