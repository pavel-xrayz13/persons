﻿namespace Persons.Abstractions
{
    public interface IQueryDispatcher
    {
        TResult Ask<TQuery, TResult>(TQuery query) where TQuery : IQuery<TResult>;
    }
}
