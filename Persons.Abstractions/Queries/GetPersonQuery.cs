﻿using System;
using Persons.Abstractions.Dtos;

namespace Persons.Abstractions.Queries
{
    public class GetPersonQuery: IQuery<PersonDto>
    {
        public Guid Id { get; set; }
    }
}
