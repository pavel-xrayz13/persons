﻿using System;
using Nancy.Hosting.Self;
using Topshelf.Logging;

namespace Persons.Service
{
    internal class Application
    {
        private readonly Uri _uri;
        private readonly NancyHost _nancyHost;
        private static readonly LogWriter _log = HostLogger.Get<Application>();

        public Application(Uri uri)
        {
            _uri = uri ?? throw new ArgumentNullException(nameof(uri));
            _nancyHost = new NancyHost(uri, new Bootstrapper());
        }

        public void Start()
        {
            _nancyHost.Start();
            _log.Info($"Starting service at {_uri.Port}");
        }

        public void Stop()
        {
            _log.Info("Stopping");
            _nancyHost.Stop();
            _nancyHost.Dispose();
        }
    }
}
