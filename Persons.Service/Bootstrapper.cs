﻿using Autofac;
using Nancy;
using Nancy.Bootstrappers.Autofac;
using Persons.Abstractions;
using Persons.Abstractions.Commands;
using Persons.Abstractions.Data;
using Persons.Abstractions.Dtos;
using Persons.Abstractions.Queries;
using Persons.Dal;
using Persons.Dal.Data;

namespace Persons.Service
{
    public class Bootstrapper : AutofacNancyBootstrapper
    {
        protected override void ConfigureApplicationContainer(ILifetimeScope container)
        {
            container.Update(builder => builder.RegisterType<DbSchemeInitializer>());
            container.Update(builder => builder.RegisterType<PersonDbConnectionFactory>().As<IDbConnectionFactory>());
        }

        protected override void ConfigureRequestContainer(ILifetimeScope container, NancyContext context)
        {
            container.Update(builder => builder.RegisterType<CreatePersonCommandHandler>().As<ICommandHandler<CreatePersonCommand>>());
            container.Update(builder => builder.RegisterDecorator<ICommandHandler<CreatePersonCommand>>((ctx, param, instance) =>
                    new CreatePersonCommandLoggedHandler(instance)));

            container.Update(builder => builder.RegisterType<CommandDispatcher>().As<ICommandDispatcher>());
            container.Update(builder => builder.RegisterType<QueryDispatcher>().As<IQueryDispatcher>());
            container.Update(builder => builder.RegisterType<GetPersonQueryHandler>().As<IQueryHandler<GetPersonQuery, PersonDto>>());
            container.Update(builder => builder.RegisterType<PersonRepository>().As<IPersonRepository>());
        }
    }
}
