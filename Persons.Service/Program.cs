﻿using System;
using System.Configuration;
using Serilog;
using Topshelf;

namespace Persons.Service
{
    class Program
    {
        static void Main(string[] args)
        {
            const string serviceName = "PersonsService";
            const string displayName = "Persons Service";

            var hostingUrl = GetHostingUrl();
            Log.Logger = new LoggerConfiguration().ReadFrom.AppSettings().CreateLogger();

            var serviceRunnerExitCode = HostFactory.Run(config =>
            {
                config.Service<Application>(service =>
                {
                    service.ConstructUsing(factory => new Application(new Uri(hostingUrl)));
                    service.WhenStarted(application => application.Start());
                    service.WhenStopped(application => application.Stop());
                });

                config.RunAsLocalSystem();
                config.SetServiceName(serviceName);
                config.SetDisplayName(displayName);
                config.UseSerilog();
            });

            var exitCode = (int)Convert.ChangeType(serviceRunnerExitCode, serviceRunnerExitCode.GetTypeCode());
            Environment.ExitCode = exitCode;
        }

        private static string GetHostingUrl()
        {
            const string defaultPortString = "9001";
            var servicePort = ConfigurationManager.AppSettings["ServicePort"] ?? defaultPortString;
            return $"http://localhost:{servicePort}";
        }
    }
}
