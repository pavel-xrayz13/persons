﻿using System;
using Autofac;
using Autofac.Core;
using Persons.Abstractions;

namespace Persons
{
    public class CommandDispatcher : ICommandDispatcher
    {
        private readonly IComponentContext _context;

        public CommandDispatcher(IComponentContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public void Execute<TCommand>(TCommand command) where TCommand : ICommand
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            var handler = _context.Resolve<ICommandHandler<TCommand>>();
            if (handler == null)
                throw new DependencyResolutionException(nameof(ICommandHandler<TCommand>));

            handler.Execute(command);
        }
    }
}
