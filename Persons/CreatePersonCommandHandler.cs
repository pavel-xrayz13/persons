﻿using System;
using Persons.Abstractions;
using Persons.Abstractions.Commands;
using Persons.Abstractions.Data;
using Persons.Abstractions.Entities;

namespace Persons
{
    public class CreatePersonCommandHandler : ICommandHandler<CreatePersonCommand>
    {
        private readonly IPersonRepository _personRepository;

        public CreatePersonCommandHandler(IPersonRepository personRepository)
        {
            _personRepository = personRepository ?? throw new ArgumentNullException(nameof(personRepository));
        }

        public void Execute(CreatePersonCommand command)
        {
            var person = Person.CreatePerson(command.Name, command.BirthDay, command.Id);
            if (person == null)
            {
                throw new ArgumentException(nameof(person));
            }

            _personRepository.Insert(person);
        }
    }
}
