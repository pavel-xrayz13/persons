﻿using System;
using System.Globalization;
using Persons.Abstractions;
using Persons.Abstractions.Commands;
using Persons.Logging;

namespace Persons
{
    public class CreatePersonCommandLoggedHandler : ICommandHandler<CreatePersonCommand>
    {
        private readonly ILog _log = LogProvider.GetCurrentClassLogger();
        private readonly ICommandHandler<CreatePersonCommand> _decorating;

        public CreatePersonCommandLoggedHandler(ICommandHandler<CreatePersonCommand> decorating)
        {
            _decorating = decorating ?? throw new ArgumentNullException(nameof(decorating));
        }

        public void Execute(CreatePersonCommand command)
        {
            _log.Info($"CreatePersonCommand: [Name: {command.Name}, BirthDay: {command.BirthDay.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)}]");
            _decorating.Execute(command);
        }
    }
}
