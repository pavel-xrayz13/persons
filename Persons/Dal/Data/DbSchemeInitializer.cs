﻿using System.Data;
using Dapper;

namespace Persons.Dal.Data
{
    public class DbSchemeInitializer
    {
        public void OnAfterDatabaseCreated(IDbConnection connection)
        {
            const string personScheme = @"create table Person
                (
                   ID UNIQUEIDENTIFIER,
                   Name varchar(100) not null,
                   BirthDay datetime not null
                )";

            connection.Execute(personScheme);
        }
    }
}
