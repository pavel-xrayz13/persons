﻿using System;
using System.Data;
using Dapper;

namespace Persons.Dal
{
    /// <summary>
    /// Для маппинга byte поля в таблице Sqlite cо свойством типа Guid
    /// </summary>
    public class GuidHandler : SqlMapper.TypeHandler<Guid>
    {
        public override Guid Parse(object value)
        {
            return new Guid(value.ToString());
        }

        public override void SetValue(IDbDataParameter parameter, Guid value)
        {
            parameter.Value = value.ToByteArray();
        }
    }
}