﻿using System;
using System.Data;
using System.Data.SQLite;
using System.IO;
using Dapper;

namespace Persons.Dal.Data
{
    public class PersonDbConnectionFactory : IDbConnectionFactory
    {
        private readonly DbSchemeInitializer _initializer;
        private readonly string _dbPath;
        private readonly string _connectionString;

        public PersonDbConnectionFactory(DbSchemeInitializer initializer)
        {
            _initializer = initializer ?? throw new ArgumentNullException(nameof(initializer));
            _dbPath = Path.Combine(Environment.CurrentDirectory, "PersonsDb.sqlite");
            _connectionString = new SQLiteConnectionStringBuilder
            {
                DataSource = _dbPath
            }.ToString();

            SqlMapper.AddTypeHandler(new GuidHandler());
        }

        public IDbConnection Create()
        {
            if(!File.Exists(_dbPath))
                OnFirstDbAccess();

            return CreateDbConnection();
        }

        private void OnFirstDbAccess()
        {
            SQLiteConnection.CreateFile(_dbPath);
            using (var dbConnection = CreateDbConnection())
            {
                dbConnection.Open();
                _initializer.OnAfterDatabaseCreated(dbConnection);
            }
        }

        private IDbConnection CreateDbConnection()
        {
            return new SQLiteConnection(_connectionString);
        }
    }
}
