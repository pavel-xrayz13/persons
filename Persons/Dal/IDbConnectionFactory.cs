﻿using System.Data;

namespace Persons.Dal
{
    public interface IDbConnectionFactory
    {
        IDbConnection Create();
    }
}
