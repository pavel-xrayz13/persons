﻿using System;
using System.Linq;
using Dapper;
using Persons.Abstractions.Data;
using Persons.Abstractions.Entities;

namespace Persons.Dal
{
    public class PersonRepository: IPersonRepository
    {
        private readonly IDbConnectionFactory _connectionFactory;

        public PersonRepository(IDbConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory ?? 
                throw new ArgumentNullException(nameof(connectionFactory));
        }

        public Person Find(Guid id)
        {
            var byteArr = id.ToByteArray();
            SqlMapper.AddTypeHandler(new GuidHandler());

            using (var cnn = _connectionFactory.Create())
            {
                cnn.Open();

                var result = cnn.Query<Person>(
                    @"SELECT ID, Name, BirthDay
                    FROM Person
                    WHERE ID = @byteArr", new { byteArr }).FirstOrDefault();
                return result;
            }
        }

        public void Insert(Person item)
        {
            const string sql = @"INSERT INTO Person 
                ( ID, Name, BirthDay ) VALUES 
                ( @ID, @Name, @BirthDay );";

            
            using (var cnn = _connectionFactory.Create())
            {
                cnn.Open();
                cnn.Query<Person>(sql, item);
            }
        }
    }
}
