﻿using System;
using Persons.Abstractions;
using Persons.Abstractions.Data;
using Persons.Abstractions.Dtos;
using Persons.Abstractions.Queries;

namespace Persons
{
    public class GetPersonQueryHandler : IQueryHandler<GetPersonQuery, PersonDto>
    {
        private readonly IPersonRepository _personRepository;

        public GetPersonQueryHandler(IPersonRepository personRepository)
        {
            _personRepository = personRepository ?? throw new ArgumentNullException(nameof(personRepository));
        }

        public PersonDto Ask(GetPersonQuery query)
        {
            var person = _personRepository.Find(query.Id);
            return person == null ? null : new PersonDto(person);
        }
    }
}
