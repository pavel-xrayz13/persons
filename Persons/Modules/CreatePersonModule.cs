﻿using System;
using Nancy;
using Nancy.ModelBinding;
using Persons.Abstractions;
using Persons.Abstractions.Commands;

namespace Persons.Modules
{
    public class CreatePersonModule : NancyModule
    {      
        public CreatePersonModule(ICommandDispatcher commandDispatcher) : base("/api/v1/")
        {
            if(commandDispatcher == null)
                throw new ArgumentNullException(nameof(commandDispatcher));

            Post["/persons"] = parameters =>
            {
                CreatePersonCommand command;
                try
                {
                    command = this.Bind<CreatePersonCommand>();
                }
                catch (ModelBindingException)
                {
                    return Response.AsText(string.Empty).WithStatusCode(HttpStatusCode.BadRequest);
                }

                try
                {
                    commandDispatcher.Execute(command);
                }
                catch (ArgumentException ae)
                {
                    return Response.AsText(ae.Message).WithStatusCode(HttpStatusCode.UnprocessableEntity);
                }
                catch (Exception e)
                {
                    return Response.AsText(e.Message).WithStatusCode(HttpStatusCode.InternalServerError);
                }

                return Response.AsText(string.Empty).WithStatusCode(HttpStatusCode.Created).WithHeader("Location", $"/api/v1/persons/{command.Id}");
            };
        }
    }
}
