﻿using System;
using Nancy;
using Nancy.ModelBinding;
using Persons.Abstractions;
using Persons.Abstractions.Dtos;
using Persons.Abstractions.Queries;

namespace Persons.Modules
{
    public class GetPersonModule : NancyModule
    {
        public GetPersonModule(IQueryDispatcher queryDispatcher) : base("/api/v1/")
        {
            if(queryDispatcher == null)
                throw new ArgumentNullException(nameof(queryDispatcher));

            Get["/persons/{id}"] = parameters =>
            {
                GetPersonQuery query;
                try
                {
                    query = this.Bind<GetPersonQuery>();
                }
                catch (ModelBindingException e)
                {
                    return Response.AsText(e.Message).WithStatusCode(HttpStatusCode.BadRequest);
                }

                PersonDto person;
                try
                {
                    person = queryDispatcher.Ask<GetPersonQuery, PersonDto>(query);
                }
                catch (Exception e)
                {
                    return Response.AsText(e.Message).WithStatusCode(HttpStatusCode.InternalServerError);
                }


                return person == null ? Response.AsText(string.Empty).WithStatusCode(HttpStatusCode.NotFound) 
                    : Response.AsJson(person).WithStatusCode(HttpStatusCode.OK);
            };
        }
    }
}
