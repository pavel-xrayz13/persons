﻿using System;
using Autofac;
using Nancy.TinyIoc;
using Persons.Abstractions;

namespace Persons
{
    public class QueryDispatcher : IQueryDispatcher
    {
        private readonly IComponentContext _context;

        public QueryDispatcher(IComponentContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public TResult Ask<TQuery, TResult>(TQuery query) where TQuery : IQuery<TResult>
        {
            if (query == null)
                throw new ArgumentNullException(nameof(query));

            var handler = _context.Resolve<IQueryHandler<TQuery, TResult>>();
            if (handler == null)
                throw new TinyIoCResolutionException(typeof(TQuery));

            return handler.Ask(query);
        }
    }
}
